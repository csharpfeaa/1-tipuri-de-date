﻿using System;

namespace Tipuri_de_date
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // declararea unei variabile: 4 parti: tip / nume / semnul de atribuire / valoare
            int varsta = 20;
            
            // concatenare
            Console.WriteLine("Varsta mea este: " + varsta);
            
            // numele variabilelor, importanta definirii sugestive a variabilelor
            // camelCase / snake_case / Pascal
            
            // siruri de caractere
            string nume = "Stefan";
            Console.WriteLine("Numele meu este: " + nume);
            
            // interpolarea unui sir de caractere
            int mere = 3;
            int pere = 2;
            int gutui = 4;

            string propozitie = "Ana are " + mere + " mere, " + pere + " pere si " + gutui + " gutui. Total " + (mere + pere + gutui) + ".";
            string propozitieOptimizata = $"Ana are {mere} mere, {pere} pere si {gutui} gutui. Total {mere + pere + gutui}.";
            Console.WriteLine(propozitie);
            Console.WriteLine(propozitieOptimizata);
            
            // numerele pot contine underscore pentru lizibilitate
            int nrLung = 1000000;
            int nrMaiLung = 1_000_000_000;
            
            // case sensitive
            // aceste 2 variabile sunt diferite
            int numarAleator = 12;
            int numaraleator = 5;
            

            // acesta este un comentariu pe o singura linie
            
            /*
             * Acestea
             * este 
             * un comentariu
             * pe mai multe
             * linii
             */

            // White spaces
            // totul codul se poate scrie pe o singura linie, variatii, etc,
            // recomandam formatare, spatiere, delimitare vizuala
           

            // operatiuni cu numere
            // % / *
            int rest = varsta % 3;
            Console.WriteLine("Rest: " + rest);
            
            // numeric literals
            // by default numarul este fie un double fir un integer: int, uint, long, ulong
            var testNr1 = 1.0; // double
            var testNr2 = 6.2F; // float
            var testNr3 = 1.0M; // decimal
            var testNr4 = 12L; // long
            Console.WriteLine($"Test nr 1: {testNr1} - {testNr1.GetType()}");
            Console.WriteLine($"Test nr 2: {testNr2} - {testNr2.GetType()}");
            Console.WriteLine($"Test nr 3: {testNr3} - {testNr3.GetType()}");
            Console.WriteLine($"Test nr 4: {testNr4} - {testNr4.GetType()}");
            
            // conversie automata la int / double
            // urmatoarea linie nu ar compila fara sufix
            float testf = 12.4F;
            
            
            // doua tipuri de conversii
            // implicite:
            // atunci cand compilatorul poate garanta ca operatiunea de conversie va fi cu succes
            // si daca nu se pierd date la conversie
            int nrStudenti = 20;
            long valImpl = nrStudenti;

            // explicit, poate fi periculos din cauza faptului ca un byte poate contine
            // numere de la 0 la 255, pe cand un int are o plaja de valori mult mai mare
            byte z = (byte)nrStudenti;

            // int division
            int nr1 = 12;
            int nr2 = 5;

            double impartireGresita1 = nr1 / nr2; // rezultat 2
            double impartireGresita2 = (double)(nr1 / nr2); // rezultat 2
            
            // este de ajuns sa convertim unul din numere la double
            double impartireCorecta = (double)nr1 / nr2;
            Console.WriteLine($"imp este {impartireCorecta}");

            // incrementare
            nr1 += 20; // echivalentul a nr1 = nr1 + 20
            Console.WriteLine($"imp este {nr1}");

            // ++ inainte / dupa
            Console.WriteLine($"Valoare varsta incrementata (++ dupa): {varsta++}");
            Console.WriteLine($"Valoare varsta: {varsta}");
            Console.WriteLine($"Valoare varsta incrementata (++ inainte): {++varsta}");
            Console.WriteLine($"Valoare varsta: {varsta}");

            // wrap around
            // byte are de la 0 - 255 (2^8)
            // putem arunca o eroare cu (checked) a*b;
            byte a = byte.MaxValue;
            a++;
            Console.WriteLine("Valoare byte: " + a);

            // siruri de caractere - dinamic, preluam din consola
            Console.Write("Care este numele tau? ");

            string numePreluat = Console.ReadLine();
            Console.WriteLine("Salut " + numePreluat + "!");

            // preluam de la user un numar
            Console.Write("Introdu numarul tau norocos: ");

            int numarNorocos = int.Parse(Console.ReadLine());
            Console.WriteLine("Numar norocos: " + numarNorocos);

            // exercitii
            // preluam 3 numere si le adunam / scadem / impartim / inmultim / %

            // aflam si media lor
            Console.Write("Introdu primul nr: ");
            int numarUser1 = int.Parse(Console.ReadLine());

            Console.Write("Introdu al doilea nr: ");
            int numarUser2 = int.Parse(Console.ReadLine());

            Console.Write("Introdu al treilea nr: ");
            int numarUser3 = int.Parse(Console.ReadLine());

            double media = (numarUser1 + numarUser2 + numarUser3) / 3;
            Console.WriteLine($"media gresita: {media}");

            double mediaCorecta = (double)(numarUser1 + numarUser2 + numarUser3) / 3;
            Console.WriteLine($"media corecta: {mediaCorecta}");

            // adaugam aceasta linie pentru ca
            // in modul debug aplicatia inchide aplicatia dupa ce termina de executat 
            Console.ReadKey();
        }
    }
}